package com.smartfridge.resource;

import com.smartfridge.service.IngredientsService;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("ingredients")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class IngredientsResource {

    private IngredientsService ingredientsService;

    public IngredientsResource(final IngredientsService ingredientService){
        this.ingredientsService = ingredientService;
    }

    @GET
    public Response getIngredients(){
        return Response.ok(ingredientsService.getIngredient()).build();
    }

}
