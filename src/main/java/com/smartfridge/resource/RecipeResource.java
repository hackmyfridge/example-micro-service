package com.smartfridge.resource;

import com.smartfridge.service.RecipeService;
import com.smartfridge.service.RecipeSorter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("recipes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RecipeResource {

    private RecipeService recipeService;

    public RecipeResource(final RecipeService recipeService){
        this.recipeService = recipeService;
    }

    @GET
    public Response getRecipes(
            @QueryParam("orderBy")
            final String order){
            RecipeSorter.Ordering ordering = RecipeSorter.Ordering.KJ;

        if (!order.isEmpty()) {
            try {
                ordering = RecipeSorter.Ordering.valueOf(order);
            } catch (final Exception ex) {
                ex.printStackTrace();
            }
        }

        return Response.ok(recipeService.getRecipes(ordering)).build();
    }

}
