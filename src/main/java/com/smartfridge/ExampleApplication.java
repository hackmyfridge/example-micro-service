package com.smartfridge;

import com.smartfridge.configuration.ExampleApplicationConfiguration;
import com.smartfridge.dao.IngredientsDao;
import com.smartfridge.dao.RecipeDao;
import com.smartfridge.resource.IngredientsResource;
import com.smartfridge.resource.RecipeResource;
import com.smartfridge.service.IngredientsService;
import com.smartfridge.service.RecipeService;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.UrlConfigurationSourceProvider;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * The main application class for the example micro service.
 */
public class ExampleApplication extends Application<ExampleApplicationConfiguration> {
    /**
     * @param args the arguments provided via the command line
     */
    public static void main(String[] args) throws Exception {
        new ExampleApplication().run(
                "server", ClassLoader.getSystemResource("configuration.yml").toString());
    }

    @Override
    public void initialize(final Bootstrap<ExampleApplicationConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(new UrlConfigurationSourceProvider());
        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new AssetsBundle("/hackMyFridgeWeb"));
        bootstrap.addBundle(new SwaggerBundle<ExampleApplicationConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(
                    final ExampleApplicationConfiguration exampleApplicationConfiguration) {
                return exampleApplicationConfiguration.getSwagger();
            }
        });
    }

    @Override
    public void run(
            final ExampleApplicationConfiguration configuration,
            final Environment environment) throws Exception {
        final IngredientsDao ingredientsDao = new IngredientsDao(configuration);
        final RecipeDao recipeDao = new RecipeDao(configuration, ingredientsDao);

        environment.jersey().register(new IngredientsResource(new IngredientsService(ingredientsDao)));
        environment.jersey().register(new RecipeResource(new RecipeService(recipeDao)));
    }
}
