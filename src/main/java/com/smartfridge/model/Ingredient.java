package com.smartfridge.model;

public class Ingredient {
    private String name;
    private Double fat;
    private Double protein;
    private Double carbohydrates;
    private Double sugar;
    private Double kj;
    private Double calories;

    private Ingredient() {}

    private Ingredient(final Builder builder) {
        this.name = builder.name;
        this.fat = builder.fat;
        this.protein = builder.protein;
        this.carbohydrates = builder.carbohydrates;
        this.sugar = builder.sugar;
        this.kj = builder.kj;
        this.calories = builder.calories;
    }

    public String getName() {
        return name;
    }

    public Double getFat() {
        return fat;
    }

    public Double getProtein() {
        return protein;
    }

    public Double getCarbohydrates() {
        return carbohydrates;
    }

    public Double getSugar() {
        return sugar;
    }

    public Double getKj() {
        return kj;
    }

    public Double getCalories() {
        return calories;
    }

    public static Builder builder(final String name){
        return new Builder(name);
    }

    public static final class Builder{
        private final String name;
        private Double fat;
        private Double protein;
        private Double carbohydrates;
        private Double sugar;
        private Double kj;
        private Double calories;

        private Builder(final String name){
            this.name = name;
        }

        public Builder setFat(final Double fat){
            this.fat = fat;
            return this;
        }

        public Builder setProtein(final Double protein){
            this.protein = protein;
            return this;
        }

        public Builder setCarbohydrates(final Double carbohydrates){
            this.carbohydrates = carbohydrates;
            return this;
        }

        public Builder setSugar(final Double sugar){
            this.sugar = sugar;
            return this;
        }

        public Builder setKj(final Double kj){
            this.kj = kj;
            return this;
        }

        public Builder setCalories(final Double calories){
            this.calories = calories;
            return this;
        }

        public Ingredient build(){
            return new Ingredient(this);
        }
    }
}

