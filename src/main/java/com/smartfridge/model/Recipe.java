package com.smartfridge.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Recipe {
    private String name;
    private List<Ingredient> ingredients;
    private String image;

    private Recipe(){
    }

    private Recipe(final Builder builder){
        this.name = builder.name;
        this.ingredients = builder.ingredients;
        this.image = builder.image;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getImage() {
        return image;
    }

    public Double getTotalFat(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getFat()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public Double getTotalProtein(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getProtein()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public Double getTotalCarbohydrates(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getCarbohydrates()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public Double getTotalSugar(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getSugar()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public Double getTotalkj(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getKj()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public Double getTotalCalories(){
        return ingredients.stream()
                .mapToDouble(
                        (ingredient) ->
                                Optional.ofNullable(ingredient.getCalories()).orElse(0.0d))
                .reduce(0.0d, Double::sum);
    }

    public static Builder builder(final String name, final String image){
        return new Builder(name, image);
    }

    public static final class Builder {
        private String name;
        private List<Ingredient> ingredients;
        private String image;

        public Builder(final String name, final String image) {
            this.name = name;
            this.ingredients = new ArrayList<>();
            this.image = image;
        }

        public Builder addIngredient(final Ingredient ingredient){
            ingredients.add(ingredient);
            return this;
        }

        public Builder addIngredients(final List<Ingredient> ingredients) {
            this.ingredients.addAll(ingredients);
            return this;
        }

        public Recipe build(){
            return new Recipe(this);
        }
    }
}
