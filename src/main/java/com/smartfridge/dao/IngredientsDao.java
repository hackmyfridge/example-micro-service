package com.smartfridge.dao;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.smartfridge.configuration.ExampleApplicationConfiguration;
import com.smartfridge.model.Ingredient;

import java.util.List;

public class IngredientsDao {

    private CloudantClient cloudantClient;

    public IngredientsDao(final ExampleApplicationConfiguration configuration){
        this.cloudantClient =  configuration.getCloudantClient();
    }

    public Ingredient getIngredient(final String name){
        final Database database = cloudantClient.database("ingredients", false);

        return database.findByIndex("{ \"selector\": { \"name\": { \"$eq\": \""+name+"\" } } }", Ingredient.class).get(0);
    }

    public List<Ingredient> getIngredients(){
        final Database database = cloudantClient.database("ingredients", false);

        return database.findByIndex("{ \"selector\": { \"exists\": { \"$eq\": \"true\" } } }", Ingredient.class);
    }
}
