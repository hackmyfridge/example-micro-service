package com.smartfridge.dao;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.smartfridge.configuration.ExampleApplicationConfiguration;
import com.smartfridge.model.Recipe;

import java.util.List;
import java.util.stream.Collectors;

public class RecipeDao {
    private CloudantClient cloudantClient;
    private IngredientsDao ingredientsDao;

    public RecipeDao(final ExampleApplicationConfiguration configuration, final IngredientsDao ingredientsDao){
        this.cloudantClient =  configuration.getCloudantClient();
        this.ingredientsDao = ingredientsDao;
    }

//    public Recipe getRecipe(){
//        final Database database = cloudantClient.database("recipes", false);
//
//        return database.findByIndex("{ \"selector\": { \"exists\": { \"$eq\": \"true\" } } }", Recipe.class);
//    }
//
    public List<Recipe> getRecipes(){
        final Database database = cloudantClient.database("recipes", false);

        final List<Recipe> recipes = database.findByIndex("{ \"selector\": { \"exists\": { \"$eq\": \"true\" } } }", Recipe.class);

        return recipes.stream().map((recipe) -> {
            final Recipe.Builder builder = Recipe.builder(recipe.getName(), recipe.getImage());

            builder.addIngredients(recipe.getIngredients().stream()
                    .map((ingredient) -> ingredientsDao.getIngredient(ingredient.getName()))
                    .collect(Collectors.toList()));

            return builder.build();
        }).collect(Collectors.toList());
    }

}
