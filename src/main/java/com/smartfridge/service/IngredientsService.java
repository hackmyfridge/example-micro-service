package com.smartfridge.service;


import com.smartfridge.dao.IngredientsDao;
import com.smartfridge.model.Ingredient;

import java.util.List;

public class IngredientsService {
    private IngredientsDao ingredientsDao;

    public IngredientsService(final IngredientsDao ingredientsDao){
        this.ingredientsDao = ingredientsDao;
    }

    public List<Ingredient> getIngredient(){
        return ingredientsDao.getIngredients();
    }

}
