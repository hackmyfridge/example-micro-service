package com.smartfridge.service;

import com.smartfridge.model.Recipe;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class RecipeSorter {
    public enum Ordering {
        FAT,
        PROTEIN,
        CARBOHYDRATES,
        SUGAR,
        KJ,
        CALORIES
    }

    private RecipeSorter(){
    }

    public static List<Recipe> sort(final Ordering order, final List<Recipe> recipes){
        return recipes.stream().sorted(
                Comparator.comparing(
                        (recipe) -> {
                            switch (order) {
                                case FAT:
                                    return recipe.getTotalFat();
                                case PROTEIN:
                                    return recipe.getTotalProtein();
                                case CARBOHYDRATES:
                                    return recipe.getTotalCarbohydrates();
                                case SUGAR:
                                    return recipe.getTotalSugar();
                                case KJ:
                                    return recipe.getTotalkj();
                                case CALORIES:
                                    return recipe.getTotalCalories();
                                default:
                                    return recipe.getTotalkj();
                            }
                        },
                        Comparator.naturalOrder())
        ).collect(Collectors.toList());
    }
}
