package com.smartfridge.service;

import com.smartfridge.dao.RecipeDao;
import com.smartfridge.model.Recipe;

import java.util.List;

public class RecipeService {
    private RecipeDao recipeDao;

    public RecipeService(final RecipeDao recipeDao){
        this.recipeDao = recipeDao;
    }

    public List<Recipe> getRecipes(final RecipeSorter.Ordering order){
        return RecipeSorter.sort(order, recipeDao.getRecipes());
    }
}
