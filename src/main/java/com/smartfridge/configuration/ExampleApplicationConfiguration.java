package com.smartfridge.configuration;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

/**
 * Configuration class for the Example micro service.
 */
public class ExampleApplicationConfiguration extends Configuration {

    @JsonProperty
    private SwaggerBundleConfiguration swagger;
    @JsonProperty
    private String cloudantUsername;
    @JsonProperty
    private String cloudantPassword;
    @JsonProperty
    private String cloudantAccount;

    public SwaggerBundleConfiguration getSwagger() {
        return swagger;
    }

    public CloudantClient getCloudantClient(){
        return ClientBuilder.account(cloudantAccount)
                .username(cloudantUsername)
                .password(cloudantPassword)
                .build();
    }
}
